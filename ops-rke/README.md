# RKE  Cluster Demo

This demo is using Ansible to operate with Kubernetes Cluster
## Usage

In `setup-rke` module, we generated kubeconfig name `kube_config_workload.yaml`

  1. Tell `kubectl` where to find the `kubeconfig`:

     ```
     $ export KUBECONFIG=../setup-rke/kube_config_workload.yaml
     ```
  2. Test that `kubectl` can see the cluster:

     ```
     $ kubectl get svc
     NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)   AGE
     kubernetes   ClusterIP   10.100.0.1   <none>        443/TCP   19m
     ```

### Deploy an application to RKE with Ansible

There is a `deploy.yml` playbook which deploys a WordPress website (using MySQL for a database) into the Kubernetes cluster.

Run the playbook to deploy the website:

    $ ansible-playbook -i inventory deploy.yml

Note the ingress require tls, we need generate it using following commands

```
KEY_FILE=aigent.key
CERT_FILE=aigent.cert
HOST=aigent.example.com
CERT_NAME=tls-aigent

openssl req -x509 -nodes -days 365 -newkey rsa:2048 -keyout ${KEY_FILE} -out ${CERT_FILE} -subj "/CN=${HOST}/O=${HOST}"

kubectl --kubeconfig kube_config_workload.yaml create secret tls ${CERT_NAME} --key ${KEY_FILE} --cert ${CERT_FILE}
```
Then we can refer it using following Kube 

```
apiVersion: networking.k8s.io/v1beta1
kind: Ingress
metadata:
  name: nginx-wp
  annotations:
    kubernetes.io/ingress.class: nginx
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
  
spec:
  tls:
    - hosts:
      - aigent.example.com
      # This assumes tls-aigent exists and the SSL
      # certificate contains a CN for aigent.example.com
      secretName: tls-aigent
  rules:
    - host: aigent.example.com
      http:
        paths:
        - path: /
          backend:
            # This assumes wordpress exists and routes to healthy endpoints
            serviceName: wordpress
            servicePort: 80

```

To use ingress, one thing to add is using helm (*)

```
helm install nginx-ingress stable/nginx-ingress --set rbac.create=true
```
(*) RKE also supports Ingress

### Troubleshooting


