### Pre-requisites

Terraform
Ansible


### Modules

##### setup-rke: 

Setup rke using Terraform.

Follow setup-rke/README.md for more information


##### ops-rke:

Deploy a wordpress application to RKE using Ansible.

Follow ops-rke/README.md for more information

#### TODOS

Add WireGuard VPN 

Add Destroy Script

Add more docs :)


#### In Action


After Creating RKE using setup-rke, Terraform will export following config file
`kube_config_workload.yaml`

KUBE_CONFIG

```
apiVersion: v1
kind: Config
clusters:
- name: "aigent-test"
  cluster:
    server: "https://rancher.34.221.149.93.xip.io/k8s/clusters/c-bglp7"
    certificate-authority-data: "LS0tLS1CRUdJTiBDRVJUSUZJQ0FURS0tLS0tCk1JSUJpRENDQ\
      VM2Z0F3SUJBZ0lCQURBS0JnZ3Foa2pPUFFRREFqQTdNUnd3R2dZRFZRUUtFeE5rZVc1aGJXbGoKY\
      kdsemRHVnVaWEl0YjNKbk1Sc3dHUVlEVlFRREV4SmtlVzVoYldsamJHbHpkR1Z1WlhJdFkyRXdIa\
      GNOTWpFdwpNekExTURjME9UQTNXaGNOTXpFd016QXpNRGMwT1RBM1dqQTdNUnd3R2dZRFZRUUtFe\
      E5rZVc1aGJXbGpiR2x6CmRHVnVaWEl0YjNKbk1Sc3dHUVlEVlFRREV4SmtlVzVoYldsamJHbHpkR\
      1Z1WlhJdFkyRXdXVEFUQmdjcWhrak8KUFFJQkJnZ3Foa2pPUFFNQkJ3TkNBQVNUdkpOa1JKbXk2N\
      VM1VFpKNVpqTFU0dE9DRkgyMkRMZFBublloNWx5YgpYUG4vNkl6ZndSeTR0QU9NOHhlZUJWWml1Y\
      zZ4UXhKSE5BNVU5TUlnVkhvVm95TXdJVEFPQmdOVkhROEJBZjhFCkJBTUNBcVF3RHdZRFZSMFRBU\
      UgvQkFVd0F3RUIvekFLQmdncWhrak9QUVFEQWdOSUFEQkZBaUVBcGdIOWlxcVUKUFljSWRINDJKT\
      EtuU2IwbllGVEdWR2haaEQ2SjM2Q0RheE1DSUJFZU1LOEtNa21nOGhOalFNRzFnRFhCdHJ2SQo5R\
      FJEbVFJakdUR1ZwWnhCCi0tLS0tRU5EIENFUlRJRklDQVRFLS0tLS0="

users:
- name: "aigent-test"
  user:
    token: "kubeconfig-user-wkw94:qrkkzxf2qv6k5q2t86fg8xrmmpv2s4xrxw8ttbz8vkt8dwkrxhh5wq"


contexts:
- name: "aigent-test"
  context:
    user: "aigent-test"
    cluster: "aigent-test"

current-context: "agient-test"

```

then we can use that kube_config by

```
export KUBECONFIG=./setup-rke/kube_config_workload.yaml
kubectl get nodes
```

expecting following output

```
 kubectl get nodesNAME            STATUS   ROLES                      AGE    VERSION
ip-10-0-0-242   Ready    controlplane,etcd,worker   114m   v1.18.12
```

In this demo, we're to use Ansible to manage Kubernetes
Checkout 

```
- hosts: localhost
  gather_facts: false

  vars_files:
    - vars/main.yml

  tasks:
    - name: Create a k8s namespace
      community.kubernetes.k8s:
        kubeconfig: '{{ k8s_kubeconfig }}'
        name: aigent-test
        api_version: v1
        kind: Namespace
        state: present 
    - name: Create a k8s monitoring namespace
      community.kubernetes.k8s:
        kubeconfig: '{{ k8s_kubeconfig }}'
        name: monitoring
        api_version: v1
        kind: Namespace
        state: present          
    - name: Deploy WordPress secrets.
      k8s:
        definition: '{{ item }}'
        kubeconfig: '{{ k8s_kubeconfig }}'
        state: present
      loop: "{{ lookup('template', 'wordpress/mysql-pass.yml') | from_yaml_all | list }}"
      no_log: "{{ k8s_no_log }}"

    - name: Deploy MySQL and WordPress.
      k8s:
        definition: '{{ item }}'
        kubeconfig: '{{ k8s_kubeconfig }}'
        state: present
      loop:
        - "{{ lookup('template', 'wordpress/mysql.yml') | from_yaml_all | list }}"
        - "{{ lookup('template', 'wordpress/wordpress.yml') | from_yaml_all | list }}"

    - name: Deploy Ingress.
      k8s:
        definition: '{{ item }}'
        kubeconfig: '{{ k8s_kubeconfig }}'
        state: present
      loop:
        - "{{ lookup('template', 'ingress/ingress-nginx-wordpress.yml') | from_yaml_all | list }}"
      tags: nginx

```
Create namespace

```
  tasks:
    - name: Create a k8s namespace
      community.kubernetes.k8s:
        kubeconfig: '{{ k8s_kubeconfig }}'
        name: aigent-test
        api_version: v1
        kind: Namespace
        state: present 
```

Create WordPress that use Mysql as database

```
    - name: Deploy MySQL and WordPress.
      k8s:
        definition: '{{ item }}'
        kubeconfig: '{{ k8s_kubeconfig }}'
        state: present
      loop:
        - "{{ lookup('template', 'wordpress/mysql.yml') | from_yaml_all | list }}"
        - "{{ lookup('template', 'wordpress/wordpress.yml') | from_yaml_all | list }}"
```        


And Expose Service using Nginx ingress

```
    - name: Deploy Ingress.
      k8s:
        definition: '{{ item }}'
        kubeconfig: '{{ k8s_kubeconfig }}'
        state: present
      loop:
        - "{{ lookup('template', 'ingress/ingress-nginx-wordpress.yml') | from_yaml_all | list }}"
      tags: nginx
```      